import {AfterViewInit, Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  title = 'siteWithControlPanel';

  constructor( public viewContainerRef: ViewContainerRef) {
  }

  ngAfterViewInit(): void {
  }

}
