import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { VideoPageComponent } from './components/pages/video-page/video-page.component';
import { TablePageComponent } from './components/pages/table-page/table-page.component';
import { PicturePageComponent } from './components/pages/picture-page/picture-page.component';
import { TextPageComponent } from './components/pages/text-page/text-page.component';
import { WorkerPageComponent } from './components/pages/worker-page/worker-page.component';
import {ControlPanelComponent} from './components/control-panel/control-panel.component';
import { VideoPanelComponent } from './components/controlPanelViews/video-panel/video-panel.component';
import { TablePanelComponent } from './components/controlPanelViews/table-panel/table-panel.component';
import { PicturePanelComponent } from './components/controlPanelViews/picture-panel/picture-panel.component';
import { TextPanelComponent } from './components/controlPanelViews/text-panel/text-panel.component';
import { WorkerPanelComponent } from './components/controlPanelViews/worker-panel/worker-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    NotFoundComponent,
    VideoPageComponent,
    TablePageComponent,
    PicturePageComponent,
    TextPageComponent,
    WorkerPageComponent,
    ControlPanelComponent,
    VideoPanelComponent,
    TablePanelComponent,
    WorkerPanelComponent,
    TextPanelComponent,
    PicturePanelComponent
  ],
  entryComponents: [
    ControlPanelComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: VideoPageComponent},
      {path: 'videoPage', component: VideoPageComponent},
      {path: 'tablePage', component: TablePageComponent},
      {path: 'picturePage', component: PicturePageComponent},
      {path: 'textPage', component: TextPageComponent},
      {path: 'workerPage', component: WorkerPageComponent},
      {path: '**', component: NotFoundComponent},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
