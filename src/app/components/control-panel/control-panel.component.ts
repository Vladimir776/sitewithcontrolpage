import {
  AfterViewInit,
  Component,
  ComponentFactory,
  ComponentFactoryResolver, EventEmitter,
  Input,
  OnInit, Output,
  Renderer2,
  Type,
  ViewChild, ViewContainerRef
} from '@angular/core';
import {PicturePanelComponent} from '../controlPanelViews/picture-panel/picture-panel.component';
// import {ControlPanelService} from '../../services/control-panel.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit, AfterViewInit {
  // фабрика компонента, который мы хотим поместить в Control Panel
  @Input() componentFactory: ComponentFactory<any>;
  // Заголовок Control Page
  @Input() title: string;
  // Находим ссылку на контейнер ng-template в Control Panel
  @ViewChild('controlPanel', {read: ViewContainerRef}) controlPanelRef: ViewContainerRef;
  constructor(
  ) {
  }

  ngAfterViewInit(): void {
    // Очищаем контейнер
    this.controlPanelRef.clear();
    // Подставляем компонент
    this.controlPanelRef.createComponent(this.componentFactory);
  }

  ngOnInit(): void {
  }

}

