import {AfterViewInit, Component, OnInit} from '@angular/core';
import {PicturePanelComponent} from '../picture-panel/picture-panel.component';

@Component({
  selector: 'app-table-panel',
  templateUrl: './table-panel.component.html',
  styleUrls: ['./table-panel.component.css', '../controlViews.css']
})
export class TablePanelComponent implements OnInit , AfterViewInit{

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

}
