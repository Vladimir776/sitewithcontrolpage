import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-text-panel',
  templateUrl: './text-panel.component.html',
  styleUrls: ['./text-panel.component.css', '../controlViews.css']
})
export class TextPanelComponent implements OnInit {
  @Output() kek = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
