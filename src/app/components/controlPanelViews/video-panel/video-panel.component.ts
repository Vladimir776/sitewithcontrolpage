import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video-panel',
  templateUrl: './video-panel.component.html',
  styleUrls: ['./video-panel.component.css', '../controlViews.css']
})
export class VideoPanelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
