import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-worker-panel',
  templateUrl: './worker-panel.component.html',
  styleUrls: ['./worker-panel.component.css', '../controlViews.css']
})
export class WorkerPanelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
