import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Page} from '../../interfaces/page';
import {ActivatedRoute, Router} from '@angular/router';

const LIST: Page[] = [
  {name: 'Страница с видео',     routerLink : 'videoPage'},
  {name: 'Страница с таблицей',  routerLink : 'tablePage'},
  {name: 'Страница с картинкой', routerLink : 'picturePage'},
  {name: 'Страница с текстом',   routerLink : 'textPage'},
  {name: 'Страница работника',   routerLink : 'workerPage'},
  ];

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent {

  // список страниц
  public menu: Page[] = LIST;

  constructor(
  ) {
  }

}
