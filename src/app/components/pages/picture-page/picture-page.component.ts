import {
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import {PicturePanelComponent} from '../../controlPanelViews/picture-panel/picture-panel.component';
import {ControlPanelService} from '../../../services/control-panel.service';

@Component({
  selector: 'app-picture-page',
  templateUrl: './picture-page.component.html',
  styleUrls: ['./picture-page.component.css']
})
export class PicturePageComponent implements OnDestroy {
  constructor(
    private controlPanelService: ControlPanelService,
  ) {
    this.controlPanelService.openControlPanel(PicturePanelComponent, 'Страница с картинкой');
  }

  ngOnDestroy(): void {
    this.controlPanelService.closeControlPanel();
  }
}
