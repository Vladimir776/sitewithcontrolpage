import {
  Component, OnDestroy,
  OnInit,
} from '@angular/core';
import {ControlPanelService} from '../../../services/control-panel.service';
import {TablePanelComponent} from '../../controlPanelViews/table-panel/table-panel.component';
@Component({
  selector: 'app-table-page',
  templateUrl: './table-page.component.html',
  styleUrls: ['./table-page.component.css']
})
export class TablePageComponent implements OnDestroy {


  constructor(
    private controlPanelService: ControlPanelService,
  ) {
    controlPanelService.openControlPanel(TablePanelComponent, 'Страница с таблцией');
  }

  ngOnDestroy(): void {
    this.controlPanelService.closeControlPanel();
  }


}
