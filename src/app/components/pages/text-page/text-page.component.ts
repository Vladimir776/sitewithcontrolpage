import {Component, OnDestroy, OnInit} from '@angular/core';
import {ControlPanelService} from '../../../services/control-panel.service';
import {TablePanelComponent} from '../../controlPanelViews/table-panel/table-panel.component';
import {TextPanelComponent} from '../../controlPanelViews/text-panel/text-panel.component';

@Component({
  selector: 'app-text-page',
  templateUrl: './text-page.component.html',
  styleUrls: ['./text-page.component.css']
})
export class TextPageComponent implements OnDestroy {

  constructor(
    private controlPanelService: ControlPanelService,
  ) {
    controlPanelService.openControlPanel(TextPanelComponent, 'Страница с текстом');
  }

  ngOnDestroy(): void {
    this.controlPanelService.closeControlPanel();
  }

}
