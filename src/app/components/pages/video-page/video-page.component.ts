import {Component, OnDestroy, OnInit} from '@angular/core';
import {ControlPanelService} from '../../../services/control-panel.service';
import {VideoPanelComponent} from '../../controlPanelViews/video-panel/video-panel.component';

@Component({
  selector: 'app-video-page',
  templateUrl: './video-page.component.html',
  styleUrls: ['./video-page.component.css']
})
export class VideoPageComponent implements OnDestroy {

  constructor(
    private controlPanelService: ControlPanelService,
  ) {
    controlPanelService.openControlPanel(VideoPanelComponent, 'Страница с видео');
  }

  ngOnDestroy(): void {
    this.controlPanelService.closeControlPanel();
  }

}
