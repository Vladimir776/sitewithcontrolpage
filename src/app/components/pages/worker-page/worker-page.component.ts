import {Component, OnDestroy, OnInit} from '@angular/core';
import {ControlPanelService} from '../../../services/control-panel.service';
import {WorkerPanelComponent} from '../../controlPanelViews/worker-panel/worker-panel.component';

@Component({
  selector: 'app-worker-page',
  templateUrl: './worker-page.component.html',
  styleUrls: ['./worker-page.component.css']
})
export class WorkerPageComponent implements OnDestroy {

  constructor(
    private controlPanelService: ControlPanelService,
  ) {
    controlPanelService.openControlPanel(WorkerPanelComponent, 'Страница сотрудника');
  }

  ngOnDestroy(): void {
    this.controlPanelService.closeControlPanel();
  }

}
