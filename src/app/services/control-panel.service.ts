import {
  ApplicationRef,
  ComponentFactory,
  ComponentFactoryResolver,
  Injectable,
  Type,
  ViewContainerRef
} from '@angular/core';
import {ControlPanelComponent} from '../components/control-panel/control-panel.component';

@Injectable({
  providedIn: 'root'
})
export class ControlPanelService {
  // tslint:disable-next-line:variable-name
  private appViewRef: ViewContainerRef;
  private readonly controlPanelContainerFactory: ComponentFactory<ControlPanelComponent>;
  private controlPanelContainer = ControlPanelComponent;
  private controlPanelContainerRef = null;

  constructor(
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) {
    // Получаем ссылку на контейнер, используя ссылку на главный компонент
    this.appViewRef = this.getRootViewContainerRef();
    this.controlPanelContainerFactory = this.componentFactoryResolver.resolveComponentFactory(this.controlPanelContainer);
  }

  private getRootViewContainerRef(): ViewContainerRef {
    // Получаем ссылку на View Container из root component
    const appInstance = this.appRef.components[0].instance;
    if (!appInstance.viewContainerRef) {
      const appName = this.appRef.componentTypes[0].name;
      throw new Error(`Missing 'viewContainerRef' declaration in ${appName} constructor`);
    }
    return appInstance.viewContainerRef;
  }
  public openControlPanel<T>(T: Type<T>, title: string) {
    // Создаем  Control Panel из фабрики и помещаем ее после главного компонента
    this.controlPanelContainerRef = this.appViewRef.createComponent(this.controlPanelContainerFactory);
    // Передаем фабрику компонента в  Control page через instance для дальнейшего отображения
    this.controlPanelContainerRef.instance.componentFactory = this.componentFactoryResolver.resolveComponentFactory(T);
    // Передаем заголовок страницы
    this.controlPanelContainerRef.instance.title = title;
  }
  public closeControlPanel() {
    this.appViewRef.clear();
  }
}
